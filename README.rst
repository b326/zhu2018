========================
zhu2018
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/zhu2018/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/zhu2018/1.1.0/

.. image:: https://b326.gitlab.io/zhu2018/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/zhu2018

.. image:: https://b326.gitlab.io/zhu2018/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/zhu2018/

.. image:: https://badge.fury.io/py/zhu2018.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/zhu2018

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/zhu2018/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/zhu2018/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/zhu2018/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/zhu2018/commits/main
.. #}

Data and formalisms from Zhu 2018

