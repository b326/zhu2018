"""
Format leaf sim/obs from fig4
"""
from pathlib import Path

import numpy as np
import pandas as pd
from graphextract.svg_extractor import extract_data

from zhu2018 import pth_clean

hours = list(range(0, 24 * 5 + 13))

dfs = []
for vname, sub_fig in [("sapflow", "a"), ("lwp", "c"), ("aba", "e")]:
    data = extract_data(f"../raw/fig_4{sub_fig}.svg", x_formatter=int, y_formatter=float)

    # obs
    records = []
    pts = sorted(data['obs'])
    for day, val in pts:
        records.append({'hour': int(day * 24 + 0.5), f'{vname}_obs': val})

    obs = pd.DataFrame(records).set_index('hour')

    # obs
    records = []
    pts = sorted(data['sim'])

    # interpolate on fixed hours
    vals = np.interp(hours, [d * 24 for d, _ in pts], [v for _, v in pts])
    for hour, val in zip(hours, vals):
        records.append({'hour': hour, f'{vname}_sim': val})

    sim = pd.DataFrame(records).set_index(['hour'])

    dfs.append(pd.concat([obs, sim], axis='columns'))

df = pd.concat(dfs, axis='columns')

# write resulting dataframe in a csv file
df = df.sort_index()
df = df[sorted(df.columns)]

with open(pth_clean / "fig4.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("# hour: number of hour post last irrigation\n")
    fhw.write("# aba: [µmol.m-3] ABA concentration\n")
    fhw.write("# lwp: [MPa] leaf water potential\n")
    fhw.write("# sapflow: [mg H20.plant-1.s-1] plant water flux\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.3f")
