"""
Fig4 (leaf sim/obs)
===================

Plot formatted data to compare with fig4
"""
import matplotlib.pyplot as plt
import pandas as pd

from zhu2018 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#", index_col=['hour'])

# plot data
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(7, 7), squeeze=False)

for ax, (vname, unit, limits) in zip(axes.flatten(), [('sapflow', "[mg H20.plant-1.s-1]", (0, 10)),
                                                      ('lwp', "[MPa]", (-1.6, 0)),
                                                      ('aba', "[µmol.m-3]", (0, 1100))]):
    ax.plot(df.index, df[f'{vname}_obs'], 'o')
    ax.plot(df.index, df[f'{vname}_sim'])

    ax.set_ylim(*limits)
    ax.set_ylabel(f"{vname} {unit}")

ticks = [(day * 24, f"{day:d}") for day in range(6)]
for ax in axes[-1, :]:
    ax.set_xticks([h for h, _ in ticks])
    ax.set_xticklabels([lab for h, lab in ticks])

fig.tight_layout()
plt.show()
