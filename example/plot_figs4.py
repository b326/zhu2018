"""
FigS4 (weather)
===============

Plot formatted data to compare with figS4
"""
import matplotlib.pyplot as plt
import pandas as pd

from zhu2018 import pth_clean

# read data
df = pd.read_csv(pth_clean / "figs4.csv", sep=";", comment="#", index_col=['hour'])

# plot data
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(14, 7), squeeze=False)

for ax, (vname, unit, limits) in zip(axes.flatten(), [('t_air', "[°C]", (18, 35)),
                                                      ('vpd', "[kPa]", (0.2, 2)),
                                                      ('rg', "[µmol photon.m-2.s-1]", (0, 2200)),
                                                      ('swc', "[kg.kg-1]", (0.12, 0.35))]):
    ax.plot(df.index, df[f'{vname}'])

    ax.set_ylim(*limits)
    ax.set_ylabel(f"{vname} {unit}")

ticks = [(day * 24, f"{day * 24:d}") for day in range(6)]
for ax in axes[-1, :]:
    ax.set_xticks([h for h, _ in ticks])
    ax.set_xticklabels([lab for h, lab in ticks])

fig.tight_layout()
plt.show()
