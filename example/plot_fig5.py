"""
Fig5 (simu result)
==================

Plot formatted data to compare with fig5
"""
import matplotlib.pyplot as plt
import pandas as pd

from zhu2018 import pth_clean

# read data
df = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#", index_col=['hour'])

# plot data
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(14, 7), squeeze=False)

for ax, (vname, unit, limits) in zip(axes.flatten(), [('par', "[µmol photon.m-2.s-1]", (0, 1300)),
                                                      ('an', "[µmol CO2.m-2.s-1]", (0, 11)),
                                                      ('transpi', "[mg.m-2.s-1]", (0, 85)),
                                                      ('gsw', "[mol H2O.m-2.s-1]", (0, 0.22))]):
    for leaf in (16, 11, 6, 1):
        ax.plot(df.index, df[f'{vname}{leaf:02d}'], label=f"leaf{leaf:d}")

    ax.legend(loc='upper right')
    ax.set_ylim(*limits)
    ax.set_ylabel(f"{vname} {unit}")

ticks = [(day * 24, f"{day:d}") for day in range(6)]
for ax in axes[-1, :]:
    ax.set_xticks([h for h, _ in ticks])
    ax.set_xticklabels([lab for h, lab in ticks])

fig.tight_layout()
plt.show()
